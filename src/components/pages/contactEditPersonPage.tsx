import { Box, Typography } from '@mui/material';
import { useParams } from 'react-router-dom';

// import { useContactEdit } from '@hooks/contacts/useContactEdit';



export const ContactEditPersonPage: React.FC = () => {
    const { id } = useParams();
    // const { contact, update } = useContactEdit(id);

    return (
    <Box p={4} overflow="auto">
        {/* {loading && <LinearProgress sx={{ width: '100%' }} />}
        {!loading && ( */}
        <Typography variant="caption">
            Displaying person info {id}
        </Typography>
        {/* )} */}
    </Box>
    );
};
