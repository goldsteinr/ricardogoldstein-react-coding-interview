import { useCallback, useEffect, useState } from 'react';

export function useContactEdit(id: string) {
  const [result, setResult] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const fetchContact = useCallback(async () => {
    // const res = await fetchContacts(paging.size * paging.currentPage, 1);

    setResult({
    });

    setIsLoading(false);
  }, []);

  useEffect(() => {
    console.log(`Getting contact by id ${id}`);
  }, [id]);

  return {
    // contact: null as IPerson,
    isLoading,
    // update: useCallback((updated: IPerson) => {
    //   console.log(updated);
    //   throw new Error('Not implemented!');
    // }, [])
  };
}
